import json
import os
import re

import numpy as np
import pandas as pd
from sqlalchemy import create_engine

from app.settings import DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME

LOCATION = '.local/zoominfo'


def camel_to_snake(name: str) -> str:
    return re.sub(r'(?<!^)(?=[A-Z])', '_', name).lower()


def main():
    companies = [f'{LOCATION}/{x}' for x in os.listdir(LOCATION) if 'company' in x]
    contacts = [f'{LOCATION}/{x}' for x in os.listdir(LOCATION) if 'contact' in x]
    pg_engine = create_engine(f'postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}')
    for idx, company in enumerate(companies):
        with open(company) as file:
            data = json.load(file).get('data', [])
            df = pd.DataFrame(data)
        rename_dict = {}
        for column in df.columns:
            rename_dict.update({
                column: camel_to_snake(column)
            })
            if 'ID' in column:
                rename_dict.update({
                    column: camel_to_snake(column).replace('_i_d', '_id')
                })
        df = df.rename(columns=rename_dict)
        if len(df) > 0:
            # region Company
            json_columns = (
                'location', 'dozi_industry', 'top_level_industry', 'universal_tagged', 'org_universal_tagged',
                'funding'
            )
            for column in json_columns:
                if column in df.columns:
                    df[column] = df[column].apply(json.dumps)
            # endregion
            df = df.replace({np.nan: None})
            df.to_sql('sdr_zoominfo_company', con=pg_engine.connect(), if_exists='append', index=False)
        if idx > 0:
            break

    for idx, contact in enumerate(contacts):
        with open(contact) as file:
            data = json.load(file).get('data', [])
            df = pd.DataFrame(data)
        rename_dict = {}
        for column in df.columns:
            rename_dict.update({
                column: camel_to_snake(column)
            })
            if 'ID' in column:
                rename_dict.update({
                    column: camel_to_snake(column).replace('_i_d', '_id')
                })
        df = df.rename(columns=rename_dict)
        if len(df) > 0:
            # region Company
            json_columns = (
                'company_address', 'dozi_industry', 'top_level_industry', 'location', 'notice_provided_info',
                'social_urls', 'certifications', 'universal_tagged', 'org_universal_tagged', 'privacy_regulation_flags'
            )
            for column in json_columns:
                if column in df.columns:
                    df[column] = df[column].apply(json.dumps)
            # endregion
            df = df.replace({np.nan: None})
            df.to_sql('sdr_zoominfo_contact', con=pg_engine.connect(), if_exists='append', index=False)
        if idx > 0:
            break


if __name__ == '__main__':
    main()
