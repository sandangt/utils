import os

LOCATION = '.local/zoominfo-metro-region'


def main():
    files = [f'{LOCATION}/{filename}' for filename in os.listdir(LOCATION)]
    remove_filepaths = [filepath for filepath in files if os.path.getsize(filepath) < 60]
    for filepath in remove_filepaths:
        os.remove(filepath)


if __name__ == '__main__':
    main()
