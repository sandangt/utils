import json
from time import sleep

import requests

URL = 'https://app.zoominfo.com/anura/zoominfo/hUnifiedCompaniesSearch'
LOCATION = '.local/zoominfo-people-details'
HEADERS = {
    'authority': 'app.zoominfo.com',
    'accept': 'application/json, text/plain, */*',
    'accept-language': 'en-US,en;q=0.9',
    'application': 'DOZI',
    'cache-control': 'no-cache',
    'content-type': 'application/json',
    'user': '31232491',
    'x-sourceid': 'ZI_FOR_SALES',
    'x-ziaccesstoken': 'eyJraWQiOiJsejJoejBYY0Fmd25jQjdBazI1TXM1MTdfYjJPeWJMUk1TbVVwa0xnVExNIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULmpxbDZqMUowMTdxejBjRWlJYnlJbGJJZGVyTUxkd3ZZNEVtX2hJSmdtODAiLCJpc3MiOiJodHRwczovL3pvb21pbmZvLWF1dGgub2t0YS5jb20vb2F1dGgyL2RlZmF1bHQiLCJhdWQiOiJhcGk6Ly9kZWZhdWx0IiwiaWF0IjoxNjY5MzQyOTk1LCJleHAiOjE2NjkzOTY5OTUsImNpZCI6IjBvYTk5ZHNtYm5BeGxldkYzNjk2IiwidWlkIjoiMDB1M2Y0d25zZEVpUkljNXg2OTciLCJzY3AiOlsicHJvZmlsZSIsIm9wZW5pZCIsImVtYWlsIl0sImF1dGhfdGltZSI6MTY2OTM0Mjk5MywiemlVc2VybmFtZSI6InNhbi5kYW5nQG9wc3dhdC5jb20iLCJzdWIiOiJzYW4uZGFuZ0BvcHN3YXQuY29tIiwiZmlyc3ROYW1lIjoiU2FuLmRhbmciLCJsYXN0TmFtZSI6IkZyZWUgVHJpYWwiLCJ6aVNlc3Npb25UeXBlIjotMywiemlHcm91cElkIjowLCJ6aVVzZXJJZCI6MzEyMzI0OTEsInppVGVuYW50SWQiOjIwMzY3ODUyLCJlbWFpbCI6InNhbi5kYW5nQG9wc3dhdC5jb20iLCJzZkFjY291bnRJZCI6IjAwMURvMDAwMDAyZlhRT0lBMiIsInppTW9uZ29Vc2VySWQiOiIzMTIzMjQ5MSIsInppUGxhdGZvcm1zIjpbIkRPWkkiLCJBRE1JTiJdfQ.N3rMyEEuIR1bRU1rE9PqQlpt2APB7eMv2C5sxGGaPZwSLOfZ6Wz5M_bNqZf-whW9wJZfL6OtEemWyY4BEVN_z56NQBDVYGpRmF9wiXjRGI00Vgo3cg7m3RAtu_bp2vRBe1V1MDaFV2z2NAE5H4M4xDiizFJXPDQeZCuIctcU7jUlSXeEt8pu9G0CnvJ1SUODR5kTWCqCtQSobgEEa5jqqkQQCwlSA3ojlrxVe3_jr5v7vc-FyrZjks68qqO0Ou0cXbF_8xB_W7g9fgYVK9rYz2v7SoXuVx4jih-HQGsLUdQeb_KwEP8O8E_vyBNc_zj4CbnO2O_TkbC9Sb4EuH3Wyg',
    'x-ziid': '_eZC0Xma6J82Vf3j4DdNIF1Kr112b3VuHCYt7g-zz8sldngbMs1ZJKOYNOhJGaf2HKMXBjXdx1tS0H3JDaKp_w',
    'x-zisession': '_eZC0Xma6J82Vf3j4DdNIF1Kr112b3VuHCYt7g-zz8sldngbMs1ZJKOYNOhJGaf2HKMXBjXdx1ux9WmuRrkYvQ3SGGWHjsOYTJQ4iGn7BlFU8nuVcdR_KqbersyAvwk0',
}

STATES = (
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'Florida',
    'Georgia',
    'Hawaii',
    'Idaho',
    'Illinois',
    'Indiana',
    'Iowa',
    'Kansas',
    'Kentucky',
    'Louisiana',
    'Maine',
    'Maryland',
    'Massachusetts',
    'Michigan',
    'Minnesota',
    'Mississippi',
    'Missouri',
    'Montana',
    'Nebraska',
    'Nevada',
    'New Hampshire',
    'New Jersey',
    'New Mexico',
    'New York',
    'North Carolina',
    'North Dakota',
    'Ohio',
    'Oklahoma',
    'Oregon',
    'Pennsylvania',
    'Rhode Island',
    'South Carolina',
    'South Dakota',
    'Tennessee',
    'Texas',
    'Utah',
    'Vermont',
    'Virginia',
    'Washington',
    'West Virginia',
    'Wisconsin',
    'Wyoming',
    'American Samoa',
    'Canal Zone',
    'Guam',
    'Puerto Rico',
    'Virgin Islands',
)


def write_state_json(page_number: int, state: str):
    print(f'page: {page_number}, state: {state}')
    payload = json.dumps({
        'contacts': [
            {
                'personId': 1429588720
            },
            {
                'personId': -2050851763
            },
            {
                'personId': -1535770446
            },
            {
                'personId': 709114720
            },
            {
                'personId': 1719304073
            },
            {
                'personId': -2123171503
            }
        ],
        'creditSource': 'GROW'
    })
    # file_path = f'{LOCATION}/company_{'-'.join(state.lower().split(' '))}_page-{page_number}.json'
    file_path = f'{LOCATION}/company_usa_page-{page_number}.json'
    response = requests.request('POST', URL, headers=HEADERS, data=payload)
    if response.status_code == 200:
        with open(file_path, 'w') as outfile:
            json.dump(response.json(), outfile)
        sleep(3)


def main():
    for page_number in range(1, 21):
        # func = partial(write_state_json, page_number)
        # with Pool(8) as process_pool:
        #     process_pool.map(func, STATES)
        write_state_json(page_number, 'USA - All')


if __name__ == '__main__':
    main()
