import json
from functools import partial
from multiprocessing import Pool
from time import sleep

import requests

URL = 'https://app.zoominfo.com/anura/zoominfo/hPeopleSearch'
LOCATION = '.local/zoominfo-europe'
HEADERS = headers = {
    'authority': 'app.zoominfo.com',
    'accept': 'application/json, text/plain, */*',
    'accept-language': 'en-US,en;q=0.9',
    'application': 'DOZI',
    'cache-control': 'no-cache',
    'content-type': 'application/json',
    'session-token': '1',
    'x-sourceid': 'ZI_FOR_SALES',
    'user': '31261000',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'x-ziaccesstoken': 'eyJraWQiOiJGaWtfUWFxeklaOUtFY0hrMW8tOURDblBaUU9iM29YaTczSG5FMlVOU1lNIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULmpacDRwbWUxRGRTNGNVVDV6amU4eVBzay1CZHA2Um1haS1EMEZuSjZfOVEiLCJpc3MiOiJodHRwczovL3pvb21pbmZvLWF1dGgub2t0YS5jb20vb2F1dGgyL2RlZmF1bHQiLCJhdWQiOiJhcGk6Ly9kZWZhdWx0IiwiaWF0IjoxNjcxMTU1OTc1LCJleHAiOjE2NzEyMDk5NzUsImNpZCI6IjBvYTk5ZHNtYm5BeGxldkYzNjk2IiwidWlkIjoiMDB1M2wyOG96eUkxUHBZU0o2OTciLCJzY3AiOlsicHJvZmlsZSIsIm9wZW5pZCIsImVtYWlsIl0sImF1dGhfdGltZSI6MTY3MTE1NTk3NCwiemlVc2VybmFtZSI6ImphY2tAd2VlbnNwYWNlLmNvbSIsInN1YiI6ImphY2tAd2VlbnNwYWNlLmNvbSIsImZpcnN0TmFtZSI6IkphY2siLCJsYXN0TmFtZSI6IkhEIiwiemlTZXNzaW9uVHlwZSI6LTMsInppR3JvdXBJZCI6MCwiemlVc2VySWQiOjMxMjYxMDAwLCJ6aVRlbmFudElkIjoyMDM4MjIzNCwiZW1haWwiOiJqYWNrQHdlZW5zcGFjZS5jb20iLCJzZkFjY291bnRJZCI6IjAwMURvMDAwMDAxcGRPdElBSSIsInppTW9uZ29Vc2VySWQiOiIzMTI2MTAwMCIsInppUGxhdGZvcm1zIjpbIkRPWkkiLCJBRE1JTiJdfQ.AOclXV7OAPGhCZ1mEyX3ME9ALnJ5205_nq6_OsWCMMHpukIC37aYwZXqKlY51TCkEt7XoTV1oLqBBIuDwfy6qDmfk5rmx-KRdOu0iguQ-foR1h9KvJqkkIOn7MCp-L4DDMVwmpUsBaViwEQNmiyJc8FBWeXjBY30jSesCqdxIB86HdLD1YR8FUNWduefXOe3w3QImYf0D-pEuiu6YBDk-Q691rXj2VxjDJ_l-sIOldvl8BC4Ji3t-T-xN38VHvuZXTeJFgJQF_o8oomzvZMNF-sGdjl23bp4lxMnGb3ITQM8HSETrXTsAJ6DVxc-zKDZvinwHNxX--ajO1jyIynrAw',
    'x-ziid': 'u8EZSDHf0d3XbobKgyriBc7m5UTd53KFuezzjI5skr9FH00GVWC9Y5UmQm4tiCuSUAh0sEiVGxajxGV877Gl4Q',
    'x-zisession': 'u8EZSDHf0d3XbobKgyriBc7m5UTd53KFuezzjI5skr9FH00GVWC9Y5UmQm4tiCuSUAh0sEiVGxaaWRfxvzAhCw3SGGWHjsOYTJQ4iGn7BlGhyyJ5YhUZD6bersyAvwk0'
}

EUROPE_COUNTRIES = (
    'Albania',
    'Andorra',
    'Austria',
    'Belarus',
    'Belgium',
    'Bosnia Herzegovina',
    'Bulgaria',
    'Croatia',
    'Cyprus',
    'Czech Republic',
    'Denmark',
    'Estonia',
    'Finland',
    'France',
    'Germany',
    'Greece',
    'Hungary',
    'Iceland',
    'Ireland',
    'Italy',
    'Latvia',
    'Liechtenstein',
    'Lithuania',
    'Luxembourg',
    'Macedonia (FYROM)',
    'Malta',
    'Moldova',
    'Monaco',
    'Netherlands',
    'Norway',
    'Poland',
    'Portugal',
    'Romania',
    'Russia',
    'San Marino',
    'Slovakia',
    'Slovenia',
    'Spain',
    'Sweden',
    'Switzerland',
    'Ukraine',
    'United Kingdom',
)


def write_state_json(page_number: int, location: str):
    # print(f'page: {page_number}, state: {state}')
    payload = json.dumps({
        'rpp': 500,
        'sortBy': 'Relevance,company_id',
        'sortOrder': 'desc,desc',
        'country': location,
        'companyPastOrPresent': '1',
        'excludeNoCompany': 'true',
        'excludeDefunctCompanies': True,
        'returnOnlyBoardMembers': False,
        'excludeBoardMembers': True,
        'contactRequirements': '',
        'confidenceScoreMin': 85,
        'confidenceScoreMax': 99,
        'isCertified': 'include',
        'inputCurrencyCode': 'USD',
        'outputCurrencyCode': 'USD',
        'page': page_number,
        'buyingCommittee': '{\'personas\':[],\'applyToSearchCriteria\':false}',
        'feature': 'People Search - UI'
    })
    file_path = f'{LOCATION}/contact_{normalize_text(location)}_page-{page_number}.json'
    # file_path = f'{LOCATION}/contact_usa_page-{page_number}.json'
    for _ in range(5):
        response = requests.request('POST', URL, headers=HEADERS, data=payload)
        if response.status_code == 200 and (resp_json := response.json()):
            if resp_json.get('data'):
                with open(file_path, 'w') as outfile:
                    json.dump(resp_json, outfile)
                sleep(3)
                return
        sleep(1)


def main():
    for page_number in range(1, 21):
        func = partial(write_state_json, page_number)
        with Pool(16) as process_pool:
            process_pool.map(func, EUROPE_COUNTRIES)


def normalize_text(text: str, seperator: str = ' ', joiner: str = '-') -> str:
    if not text:
        return ''
    altered_text = text.lower()
    for ignore_char in ('(', ')', '[', ']', '{', '}'):
        altered_text = altered_text.replace(ignore_char, '')
    altered_text = joiner.join(altered_text.split(seperator))
    while '--' in altered_text:
        altered_text = altered_text.replace('--', '-')
    return altered_text


if __name__ == '__main__':
    main()
