import pandas as pd
from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.dialects import postgresql


def main():
    def _handle_case(input_string):
        if not input_string:
            return input_string
        result = []
        for i in input_string.split(','):
            result.append('_'.join([j.lower() for j in i.strip().split(' ')]))
        return ','.join(result)

    df = pd.read_json('.local/all-locations.json')
    df = df.drop(columns=['id']).rename(columns={'google_id': 'id', 'google_parent_id': 'parent_id'}).set_index(
        ['id'])
    df['updated_at'] = datetime.now()
    df['created_at'] = datetime.now()
    df['code'] = df['name']
    df[['code', 'canonical_name', 'country_code', 'target_type']] = df[
        ['code', 'canonical_name', 'country_code', 'target_type']].applymap(_handle_case)
    pg_engine = create_engine('postgresql+psycopg2://postgres:admin@localhost:5432/metacrawler_dev')
    df.to_sql('search_place', con=pg_engine, if_exists='replace', dtype={'gps': postgresql.JSONB})
    print(df)


if __name__ == '__main__':
    main()
