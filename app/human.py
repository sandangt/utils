class Human:
    def __init__(self, name: str, age: int):
        self._name = name
        self._age = age

    def __new__(cls, name: str, age: int):
        return super(Human, cls).__new__(cls)

    @property
    def name(self) -> str:
        return self._name

    @property
    def age(self) -> int:
        return self._age


human = Human('a', 12)
