import re
from typing import List

from sqlalchemy import Column, BigInteger, text, Text, String, Boolean, DateTime, create_engine
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()
metadata = Base.metadata


class SdrUploadpage(Base):
    __tablename__ = 'sdr_uploadpage'
    id = Column(BigInteger, primary_key=True, server_default=text("nextval('sdr_uploadpage_id_seq'::regclass)"))
    name = Column(String(500))
    website = Column(String(500))
    upload_uri = Column(String(5000), nullable=False, unique=True)
    email_addresses = Column(JSONB(astext_type=Text()))
    suggested_email_addresses = Column(JSONB(astext_type=Text()))
    filtered_email_addresses = Column(JSONB(astext_type=Text()))
    phonenumbers = Column(JSONB(astext_type=Text()))
    suggested_phonenumbers = Column(JSONB(astext_type=Text()))
    status = Column(String(20), nullable=False)
    filtered = Column(Boolean, nullable=False)
    hidden_upload = Column(Boolean)
    contacted = Column(Boolean, nullable=False)
    contacted_meta = Column(JSONB(astext_type=Text()))
    updated_at = Column(DateTime(True), nullable=False)
    created_at = Column(DateTime(True), nullable=False)
    locations = Column(JSONB(astext_type=Text()))
    screenshot_uris = Column(JSONB(astext_type=Text()))


def filter_target_emails(email_list: List[str]) -> List[str]:
    if not email_list:
        return email_list
    result = []
    for email in email_list:
        if re.search('.(js|jpe|png|jpg|jpeg|gif|avi|mp4|mp3)$', email) is not None or \
                re.search('.@(example|domain).[a-zA-Z]{2,3}$', email) is not None or \
                re.search('.(xyz.com|sentry.io)$', email) is not None or \
                'email@gmail.com' in email:
            continue
        result.append(email)
    return result


def main():
    BATCH_SIZE = 500
    pg_engine = create_engine(f'postgresql+psycopg2://postgres:admin@localhost:5432/metacrawler_dev')
    Session = sessionmaker(bind=pg_engine, autoflush=False, autocommit=False)
    with Session.begin() as session:
        uploadpage_cnt = session.query(SdrUploadpage).count()
        for offset in range(0, uploadpage_cnt, BATCH_SIZE):
            uploadpages = session.query(SdrUploadpage).slice(offset, offset+BATCH_SIZE).all()
            for uploadpage in uploadpages:
                filtered_emails = filter_target_emails(uploadpage.suggested_email_addresses)
                uploadpage.filtered_email_addresses = filtered_emails
                uploadpage.suggested_email_addresses = filtered_emails
            session.add_all(uploadpages)
        session.commit()
    print('DONE')


if __name__ == '__main__':
    main()
