import pandas as pd
from sqlalchemy import create_engine

DB_USER = 'postgres'
DB_PASSWORD = 'admin'
DB_NAME = 'metacrawler_dev'
DB_PORT = 5432
DB_HOST = 'localhost'


def main():
    pg_engine = create_engine(f'postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}')
    query = '''
        select
            distinct tab0.website,
            tab1.company_name,
            tab0.locations,
            tab1.top_level_industry
        from
            sdr_uploadpage tab0
        join sdr_zoominfo_company tab1 on
            tab0.website = tab1.company_domain
        where
            tab0.verified_source = 'ZOOMINFO'
    '''
    df = pd.read_sql(query, con=pg_engine)\
           .explode('locations')\
           .explode('top_level_industry')
    print(df)


if __name__ == '__main__':
    main()
